

var fs = require('fs');
var readline = require('readline');
var google = require('googleapis');
var googleAuth = require('google-auth-library');

// If modifying these scopes, delete your previously saved credentials
// at ~/.credentials/sheets.googleapis.com-nodejs-quickstart.json
var SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly'];
var TOKEN_DIR = (process.env.HOME || process.env.HOMEPATH ||
    process.env.USERPROFILE) + '/.credentials/';
var TOKEN_PATH = TOKEN_DIR + 'sheets.googleapis.com-nodejs-quickstart.json';

// Load client secrets from a local file.
fs.readFile('client_secret.json', function processClientSecrets(err, content) {
  if (err) {
    console.log('Error loading client secret file: ' + err);
    return;
  }
  // Authorize a client with the loaded credentials, then call the
  // Google Sheets API.
  authorize(JSON.parse(content), fetchDictionary);
});

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 *
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
  var clientSecret = credentials.installed.client_secret;
  var clientId = credentials.installed.client_id;
  var redirectUrl = credentials.installed.redirect_uris[0];
  var auth = new googleAuth();
  var oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectUrl);

  // Check if we have previously stored a token.
  fs.readFile(TOKEN_PATH, function(err, token) {
    if (err) {
      getNewToken(oauth2Client, callback);
    } else {
      oauth2Client.credentials = JSON.parse(token);
      callback(oauth2Client);
    }
  });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 *
 * @param {google.auth.OAuth2} oauth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback to call with the authorized
 *     client.
 */
function getNewToken(oauth2Client, callback) {
  var authUrl = oauth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES
  });
  console.log('Authorize this app by visiting this url: ', authUrl);
  var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });
  rl.question('Enter the code from that page here: ', function(code) {
    rl.close();
    oauth2Client.getToken(code, function(err, token) {
      if (err) {
        console.log('Error while trying to retrieve access token', err);
        return;
      }
      oauth2Client.credentials = token;
      storeToken(token);
      callback(oauth2Client);
    });
  });
}

/**
 * Store token to disk be used in later program executions.
 *
 * @param {Object} token The token to store to disk.
 */
function storeToken(token) {
  try {
    fs.mkdirSync(TOKEN_DIR);
  } catch (err) {
    if (err.code != 'EEXIST') {
      throw err;
    }
  }
  fs.writeFile(TOKEN_PATH, JSON.stringify(token));
  console.log('Token stored to ' + TOKEN_PATH);
}

/**
 * Fetches Solresol dictionary:
 * https://docs.google.com/spreadsheets/d/1l6UfoFwSB58tu39QYoVm3D9HUS_SlQ5l6hwBND2h350/edit
 * @param {google.auth.OAuth2} auth The OAuth2 client.
 */
function fetchDictionary(auth) {
  var sheets = google.sheets('v4');
  sheets.spreadsheets.values.get({
    auth: auth,
    spreadsheetId: '1l6UfoFwSB58tu39QYoVm3D9HUS_SlQ5l6hwBND2h350',
    range: 'First Part!A5:D',
  }, function(err, response) {
    if (err) {
      console.log('The API returned an error: ' + err);
      return;
    }
    var rows = response.values;
    if (rows.length == 0) {
      console.log('No data found.');
    } else {
      let section = {title: "", words: []};
      let sections = [section];
      for (var i = 0; i < rows.length; i++) {
        var row = rows[i];
        if (row.length === 1) {
          // new section
          section = {title: row[0], words: []};
          sections.push(section);
        } else {
          let [solresol, french, english, comments] = row;
          if (solresol !== undefined) {
            let word = {
              solresol, french, english, comments, section
            };
            section.words.push(word);
          }
        }
      }

      compileDictionary(sections);
    }
  });
}

class Dictionary {
  constructor() {
    this._table = new Map();
  }

  put(x, y) {
    let ys = Array.isArray(y) ? y : [y];
    let old = this._table.get(x);
    if (old !== undefined) {
      // console.log("duplicate definition for", x, ":", old, ys);
      old.push(...ys);
    } else {
      this._table.set(x, ys);
    }
  }

  get(x) {
    return this._table.get(x);
  }
}

function compileDictionary(sections) {
  var solresolEnglish = new Dictionary();
  var englishSolresol = new Dictionary();

  for (let section of sections) {
    for (let word of section.words) {
      if (word.english === undefined) {
        console.log(word);
        continue;
      }

      let englishs = word.english.toLowerCase()
        .split(', ')
        .map(x => x.trim())
        .filter(x => x.length > 0);
      solresolEnglish.put(word.solresol, englishs);

      for (let e of englishs) {
        englishSolresol.put(e, word.solresol);
      }
    }
  }
}